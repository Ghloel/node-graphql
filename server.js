var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');
var cors = require('cors');

// Initialize a GraphQL schema
var schema = buildSchema(`
  type Query {
    hello: String
    author(id: String!): Author
    authors(name: String): [Author]
  },
  type Author {
    id: String
    name: String
    email: String  
  }
`);


var authors = [
    { id: '88d6bec2', name: 'Xavier Decuyper', email: 'xavier@awesomeblog.com' },
    { id: '77e2448a', name: 'Jessie Baker', email: 'jessie@awesomeblog.com' },
    { id: '0beb564c', name: 'Adam Richards', email: 'adam@awesomeblog.com' }
];

var blogPosts = [
    {
        id: 1,
        title: 'My first blog post',
        content: 'This is my very first blog post. Hope you like it!',
        author: '88d6bec2',
    },
    {
        id: 2,
        title: 'Second blog post',
        content: 'Back for another round!',
        author: '0beb564c',
    },
    {
        id: 3,
        title: 'Building a REST API',
        content: 'A pratical guide on how to build your own REST API.',
        author: '77e2448a',
    }
];

var comments = [
    { id: 1, postId: 1, name: 'Anonymous', content: 'Good luck with your blog!' },
    { id: 2, postId: 1, name: 'Nick', content: 'Great first article. Do you have an RSS feed?' },
    { id: 3, postId: 3, name: 'Peter', content: 'You should check out GraphQL. It\'s way better and Savjee has a great tutorial on it!' },
];

var getBlogPosts = function () {
    return blogPosts
};

var getBlogPost = function(args) {
    var id = args.id;
    return blogPosts.filter(post => {
        return post.id === id;
    })[0];
};

var getCommentsForPost = function(args) {
    var postId = args.id;
    return comments.filter(comment => {
        return comment.postId === postId;
    });
};

var getAuthor = function(args) { // return a single user based on id
    var authorID = args.id;
    return authors.filter(author => {
        return author.id === authorID;
    })[0];
};

var getAuthors = function(args) { // Return a list of users. Takes an optional gender parameter
    if(args.name) {
        var name = args.name;
        return name.filter(author => author.name === name);
    } else {
        return authors;
    }
};

var getPostsOfAuthor = function(args) {
    var authorId = args.id;
    return getBlogPost.filter(item => {
        return item.author === authorId
    });
};

// Root resolver
var root = {
    blogPosts: getBlogPosts,
    blogPost: getBlogPost,
    CommentsForPost:getCommentsForPost,
    author: getAuthor,
    authors: getAuthors,
    PostsOfAuthor: getPostsOfAuthor,
    hello: () => 'Hello world!'
};

// Create an express server and a GraphQL endpoint
const app = express();

app.use(cors());

app.use('/graphql', graphqlHTTP({
    schema: schema,  // Must be provided
    rootValue: root,
    graphiql: true,  // Enable GraxphiQL when server endpoint is accessed in browser
}));

app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));
